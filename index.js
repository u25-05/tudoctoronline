const express = require("express");
//const conectarDB = require("./config/db");
const conectarDB = require("./src/conexBD/conn");
const cors = require("cors");

//crear el servidor
const app = express();

//conectar a la base de datos
conectarDB();

//habilitar cor
app.use(cors());

//Habilite express.json
app.use(express.json({ extended: true }));

const PORT = process.env.PORT || 4000; //puerto para atlas
/* const PORT = process.env.PORT || 5000; //puerto para heroku */

//importar rutas
/* app.use("/api/usuarios", require("./routes/usuarios")); */
app.use("/api/usuarios", require("./src/routers/Login/usuarios"));
/* app.use("/api/auth", require("./routes/auth")); */
app.use("/api/auth", require("./src/routers/Login/auth"));
app.use("/api/citas", require("./src/routers/cita_routers"));
/* app.use("/api/proyectos", require("./routes/proyectos"));
app.use("/api/tareas", require("./routes/tareas")); */

// arrancar la app
app.listen(PORT, () => {
  console.log(`El servidor está funcionando en el puerto ${PORT} `);
});