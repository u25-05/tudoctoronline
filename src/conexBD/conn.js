const mongoose = require("mongoose");
require("dotenv").config({ path: "variables.env" });

const conectarDB = async () => {
  console.log("sin entrar al try")
  try {
    console.log("entra al try")
    await mongoose.connect(process.env.DB_MONGO, {
      
      useNewUrlParser: true,
      useUnifiedTopology: true,
      /* useFindAndModify: false, */
    });
    console.log("DB Conectada");
  } catch (error) {
    console.log("entra al catch")
    console.log(error);
    process.exit(1);
  }
};

module.exports = conectarDB;
