var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PersonaSchema = Schema({
    genero:String,
    is_active:String,
    is_admin:String,
    nombre:String,
    user_name:String,
    type:String
});

const Persona = mongoose.model('persona', PersonaSchema);
module.exports = Persona;

//otra prueba 
