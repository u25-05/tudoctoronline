const personaController = require("../controllers/controllerPersona");
const { Router } = require("express");
const router = Router();

router.get('/buscar/:id',personaController.buscarDataId);
router.get('/buscarall/:idb?', personaController.listarAllData);
router.post('/crearPersona', personaController.savePersona);
router.delete('/borrar/:id',personaController.deletePersona);
router.put('/actualizar/:id',personaController.updatePersona);


module.exports =  router;