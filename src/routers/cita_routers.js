const { Router } = require("express");
const controllerCita = require("../controllers/controllerCitas")


const router = Router();
const auth = require("../middleware/auth");
const { check } = require("express-validator");
/* const express = require("express");
const router = express.Router(); */

/* router.get("/prueba", controllerCita.prueba); */
/* router.post('/creacion', controllerCita.saveCita); */
router.post(
  "/",
  auth,
  [check("nombre", "El nombre del proyecto es obligatorio").not().isEmpty()],
  controllerCita.crearCita
);

router.get("/", auth, controllerCita.obtenerCitas);
/* router.get("/buscarId/:id", controllerCita.buscarData); */

router.put(
  "/:id",
  auth,
  [check("nombre", "El nombre del proyecto es obligatorio").not().isEmpty()],
  controllerCita.actualizarCita
);

router.delete("/:id", auth, controllerCita.eliminarCita);

/* router.get('/buscarTodos/:idb?', controllerCita.listarAllData);
router.get('/buscarId/:id', controllerCita.buscarData);
router.put('/actualizacion/:id',controllerCita.upDateCita);
router.delete('/eliminar/:id', controllerCita.deleteCita);
 */ /* export default router; */
module.exports = router;
