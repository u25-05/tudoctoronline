const Tratamiento = require("../models/tratamiento");

function saveTratamiento(req, res) {
    var newTratamiento = new Tratamiento(req.body);
    newTratamiento.save((err, result) => {
        res.status(200).send({message:result});
    });
}

function buscarDataId(req, res) {
    var idTratamiento = req.params.id;
    Tratamiento.findById(idTratamiento).exec((err, result) => {
        if(err) {
            res
            .status(500)
            .send({message: 'Error al momento de ejecutar la solicitud'});
        } else {
            if(!result) {
                res
                .status(404)
                .send({message: 'El registro a buscar no se encuentra disponible'});
            } else {
                res.status(200).send({result});
            }
        }
    });
}


function listarAllData(req, res) {
    var idTratamiento = req.params.id;
    if(!idTratamiento) {
        var result = Tratamiento.find({}).sort('nombre');
    } else {
        var result = Tratamiento.find({Tratamiento:idTratamiento}).sort('nombre');
    }
    result.exec(function(err, result) {
        if(err) {
            res.status(500).send({message: 'Error al momento de ejecutar la solicitud'});
        } else {
            if(!result) {
                res.status(404).send({message: 'El registro a buscar no se encuentra disponible'});
            } else {
                res.status(200).send({result});
            }
        }
    });
}

function updateTratamiento(req,res) {
    var id = req.params.id;
    Tratamiento.findByIdAndUpdate({_id:id}, req.body, {new:true},
        function(err,Tratamiento) {
        if(err){
            res.send(err);
        }
        res.json(Tratamiento);
    });
}

function deleteTratamiento(req, res) {
    var idTratamiento = req.params.id;
    Tratamiento.findByIdAndDelete(idTratamiento, function (err, tratamiento) {
        if (err) {
            return res.json(500, {
                message: 'No encontramos este tratamiento',
                error: err.toString()
            });
        }
        return res.json(tratamiento);
    });
}



module.exports = {
    saveTratamiento,
    listarAllData,
    buscarDataId,
    updateTratamiento,
    deleteTratamiento
}