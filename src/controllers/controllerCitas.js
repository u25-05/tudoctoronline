const Cita = require("../models/cita");
const { validationResult } = require("express-validator");

exports.crearCita = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(401).json({ errores: errores.array() });
  }

  try {
    //crear un nuevo Cita
    const cita = new Cita(req.body);

    cita.creador = req.usuario.id;

    cita.save();
    res.json(cita);
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

exports.obtenerCitas = async (req, res) => {
  try {
    const citas = await Cita.find({ creador: req.usuario.id }).sort({
      creado: -1,
    });
    res.json({ citas });
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

exports.actualizarCita = async (req, res) => {
  //revisar si hay errores
  const errores = validationResult(req);
  if (!errores.isEmpty()) {
    return res.status(401).json({ errores: errores.array() });
  }

  const { nombre } = req.body;
  const nuevoCita = {};

  if (nombre) {
    nuevoCita.nombre = nombre;
  }

  try {
    let cita = await Cita.findById(req.params.id);

    if (!cita) {
      return res.status(400).json({ msg: "Cita no encontrado" });
    }

    if (cita.creador.toString() !== req.usuario.id) {
      return res.status(400).json({ msg: "No autorizado" });
    }

    cita = await Cita.findByIdAndUpdate(
      { _id: req.params.id },
      { $set: nuevoCita },
      { new: true }
    );

    res.json({ cita });
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};

exports.eliminarCita = async (req, res) => {
  try {
    let cita = await Cita.findById(req.params.id);

    if (!cita) {
      return res.status(400).json({ msg: "Cita no encontrado" });
    }

    if (cita.creador.toString() !== req.usuario.id) {
      return res.status(400).json({ msg: "No autorizado" });
    }

    await Cita.remove({ _id: req.params.id });
    res.json({ msg: "Cita eliminada" });
  } catch (error) {
    console.log("Hubo un error");
    console.log(error);
    res.status(400).send("Hubo un error");
  }
};
