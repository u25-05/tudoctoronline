const Persona = require("../models/persona");

function savePersona(req, res) {
    var newPerson = new Persona(req.body);
    newPerson.save((err, result) => {
        res.status(200).send({message:result});
    })
}

function buscarDataId(req, res) {
    var idPersona = req.params.id;
    Persona.findById(idPersona).exec((err, result) => {
        if(err) {
            res
            .status(500)
            .send({message: 'Error al momento de ejecutar la solicitud'});
        } else {
            if(!result) {
                res
                .status(404)
                .send({message: 'El registro a buscar no se encuentra disponible'});
            } else {
                res.status(200).send({result});
            }
        }
    });
}


function listarAllData(req, res) {
    var idPersona = req.params.id;
    if(!idPersona) {
        var result = Persona.find({}).sort('nombre');
    } else {
        var result = Persona.find({Persona:idPersona}).sort('nombre');
    }
    result.exec(function(err, result) {
        if(err) {
            res.status(500).send({message: 'Error al momento de ejecutar la solicitud'});
        } else {
            if(!result) {
                res.status(404).send({message: 'El registro a buscar no se encuentra disponible'});
            } else {
                res.status(200).send({result});
            }
        }
    });
}

function updatePersona(req,res) {
    var id = req.params.id;
    Persona.findByIdAndUpdate({_id:id}, req.body, {new:true},
        function(err,Cita) {
        if(err){
            res.send(err);
        }
        res.json(Cita);
    });
}

function deletePersona(req, res) {
    var idPersona = req.params.id;
    Persona.findByIdAndDelete(idPersona, function (err, personaRemoved) {
        if (err) {
            return res.json(500, {
                message: 'No encontramos esta persona',
                error: err.toString()
            });
        }
        return res.json(personaRemoved);
    });
}



module.exports = {
    savePersona,
    listarAllData,
    buscarDataId,
    updatePersona,
    deletePersona
}